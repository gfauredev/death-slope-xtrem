// __tests__/index.test.ts
import request from 'supertest';
import server from '../src/index';

describe('Big 5D Hardcore Game', () => {
  afterAll((done) => {
    server.close(done);
  });

  it('responds with "Hello, World!" at the root URL', async () => {
    const response = await request(server).get('/hello');
    expect(response.text).toEqual('Hello, World!');
  });
});
