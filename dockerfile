FROM node:21-alpine

RUN apk update && apk upgrade && npm install -g pnpm 

WORKDIR /var/death-slope-xtrem/

COPY . .

RUN pnpm install

EXPOSE 8080

ENTRYPOINT pnpm start
