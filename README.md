# Death Slope Xtrem

Jeu 2D hardcore gaming, avec la partie serveur en plus !

## Installation et utilisation via `Docker`

`docker` doit être installé pour utiliser ceci.

Exécuter `docker run gfauredev/death-slope-xtrem` pour télécharger et lancer le service.

Il est accessible à l’adresse `localhost:8080`.

## Installation et utilisation en local via `pnpm`

`pnpm` doit être installé pour utiliser ceci.

Exécuter `pnpm install` pour installer les dépendances,
puis `pnpm start` pour lancer le serveur web (depuis ce répertoire).

### Test

Exécuter `pnpm test` pour lancer les tests.

## Intégration continue

À chaque `push` sur le GitLab, des tests unitaires extrêmement exhaustifs et profonds sont effectués.
