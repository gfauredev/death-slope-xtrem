// src/index.ts
import express from 'express';
import http from 'http';
import path from 'path';

const app = express();
const server = http.createServer(app);
const port = 8080;
const root = path.join(__dirname, '../app');

app.use(express.static(root));

app.get('/', (req, res) => {
  res.sendFile('/game.html', { root: root });
});

app.get('/auth', (req, res) => {
  res.send('Why do you end up on /auth !?');
});

app.get('/hello', (req, res) => {
  res.send('Hello, World!');
});

server.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});

export default server;
